#![allow(non_snake_case)]
#![feature(proc_macro_hygiene, decl_macro)]

extern crate crypto;
extern crate curv;
extern crate multi_party_ecdsa;
extern crate paillier;
extern crate reqwest;
#[macro_use]
extern crate serde_derive;
extern crate hex;
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;

extern crate serde_json;

extern crate base64;
extern crate futures;
extern crate libp2p;
extern crate tokio;
extern crate tokio_core;

use rocket::config::{Config, Environment, LoggingLevel};
use rocket::post;
use rocket::State;
use rocket_contrib::json::Json;

use self::libp2p::floodsub::protocol::FloodsubMessage;
use std::collections::HashMap;
use std::process;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::sync::Mutex;

use self::crypto::digest::Digest;
use crypto::sha2::Sha256;
use curv::cryptographic_primitives::proofs::sigma_correct_homomorphic_elgamal_enc::HomoELGamalProof;
use curv::cryptographic_primitives::proofs::sigma_dlog::DLogProof;
use curv::cryptographic_primitives::secret_sharing::feldman_vss::VerifiableSS;
use curv::elliptic::curves::traits::*;
use curv::BigInt;
use curv::{FE, GE};
use multi_party_ecdsa::net::communication::Comm;

use multi_party_ecdsa::net::p2pnet;

use multi_party_ecdsa::protocols::multi_party_ecdsa::gg_2018::mta::*;
use multi_party_ecdsa::protocols::multi_party_ecdsa::gg_2018::party_i::*;

use paillier::*;
use std::env;
use std::fs;
use std::thread;
use std::time::Duration;

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct AEAD {
    pub ciphertext: Vec<u8>,
    pub tag: Vec<u8>,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct PartySignup {
    pub number: u32,
    pub uuid: String,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct Index {
    pub key: TupleKey,
}

#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct LocalMsg {
    Nodeid: String,
    Msg: String,
}

pub struct LocalServerChan {
    MsgQ: Sender<LocalMsg>,
    MsgR: Receiver<LocalMsg>,
}
#[derive(Clone, PartialEq, Debug, Serialize, Deserialize)]
pub struct GreetingMsg {
    IP: String,
    Msg: String,
}

#[derive(Serialize, Deserialize, Default, Debug)]
pub struct Params {
    parties: String,
    threshold: String,
    bootstrapnode: String,
    keygenserver: String,
    signerserver: String,
    partynum: String,
    nodeid: String,
}

#[derive(Serialize, Deserialize)]
pub struct SignaturePack {
    pub R: String,
    pub S: String,
    pub Pubkeyx: String,
    pub Pubkeyy: String,
    pub Msghash: String,
}
const WAITTIME: u64 = 10000;
const SECP256K1_KEYSIZE: usize = 32;
fn main() {
    if env::args().nth(4).is_some() {
        errormsg("too many arguments".to_owned());
        process::exit(0);
    }
    if env::args().nth(3).is_none() {
        errormsg("too few arguments".to_owned());
        process::exit(0);
    }

    let paramdata = fs::read_to_string(env::args().nth(2).unwrap()).unwrap_or_default();
    if paramdata.len() == 0 {
        errormsg("cannot find the param configure file!".to_owned());

        process::exit(0);
    }
    let params: Params = serde_json::from_str(&paramdata).unwrap_or_default();
    if params.nodeid.len() == 0 {
        errormsg("error in file format in paramsdocker".to_owned());
        process::exit(0);
    }
    // expect("error in file format in paramsdocker");
    let THRESHOLD: u32 = params.threshold.parse::<u32>().unwrap_or_default();
    let PARTIES: u32 = params.parties.parse::<u32>().unwrap_or_default();
    if THRESHOLD == 0 || PARTIES == 0 {
        errormsg("error in file format threshold and parties".to_owned());
        process::exit(0);
    }

    let BOOTSTRAPNODE = params.bootstrapnode;
    let LOCALSERVER = params.signerserver;

    let myidstring = params.partynum;
    let data = fs::read_to_string(env::args().nth(1).unwrap()).unwrap_or_default();
    if data.len() == 0 {
        errormsg("cannot find the key file!".to_owned());
        process::exit(0);
    }
    //this is sign sequence value for sign process
    // may different from the nodeid which is sequence in the key generation sequence.
    //we do not use the nodeid current in the code now.
    let (party_keys, shared_keys, _, vss_scheme_vec, paillier_key_vector, y_sum): (
        Keys,
        SharedKeys,
        u32,
        Vec<VerifiableSS>,
        Vec<EncryptionKey>,
        GE,
    ) = serde_json::from_str(&data).unwrap();

    let localip = env::args().nth(3).unwrap_or_default();
    if localip.len() == 0 {
        errormsg("error in parse the ip address".to_owned());
        process::exit(0);
    }
    // we add the part_num_int as previous we count signer from 1.
    let party_num_int: u32 = myidstring.parse().unwrap();
    let parames = Parameters {
        threshold: THRESHOLD as usize,
        parties_num: PARTIES as usize,
        bootstrapnode: BOOTSTRAPNODE,
        myip: String::from(localip),
        myidoncurve: party_num_int,
        mynodeid: params.nodeid,
    };

    let (p2ptx, p2prx): (
        tokio::sync::mpsc::Sender<String>,
        tokio::sync::mpsc::Receiver<String>,
    ) = tokio::sync::mpsc::channel(1000);

    let (p2presptx, p2presprx): (Sender<FloodsubMessage>, Receiver<FloodsubMessage>) =
        mpsc::channel();

    let (synctx, syncrx): (Sender<FloodsubMessage>, Receiver<FloodsubMessage>) = mpsc::channel();

    let (tcptx, tcprx): (mpsc::Sender<Msg>, mpsc::Receiver<Msg>) = mpsc::channel();
    thread::spawn(move || {
        p2pnet::run_tcp_server("0.0.0.0:3433".to_owned(), tcptx);
    });
    let addr = parames.bootstrapnode.to_owned();

    let sk = party_keys.u_i.get_element();
    let skptr = sk.clone().as_ptr();
    let mut p2pskarray: [u8; SECP256K1_KEYSIZE] = [0; SECP256K1_KEYSIZE];
    let slice = unsafe { std::slice::from_raw_parts(skptr, SECP256K1_KEYSIZE) };
    for i in 0..SECP256K1_KEYSIZE {
        p2pskarray[i] = slice[i];
    }
    let p2pconn = p2pnet::P2Pinstance::new(parames.myip.as_str(), addr.as_ref(), p2pskarray);

    thread::spawn(move || {
        p2pnet::run_p2p_server(p2pconn, p2presptx.clone(), synctx.clone(), p2prx);
    });

    let (MsgQtx, MsgQrx): (Sender<LocalMsg>, Receiver<LocalMsg>) = mpsc::channel();
    let (MsgAtx, MsgARx): (Sender<LocalMsg>, Receiver<LocalMsg>) = mpsc::channel();

    let localserverchan = LocalServerChan {
        MsgQ: MsgQtx,
        MsgR: MsgARx,
    };
    let _ = thread::spawn(move || {
        RunLocalserver(LOCALSERVER, localserverchan);
    });
    loop {
        let ret = signmessage(
            &tcprx,
            &p2presprx,
            &syncrx,
            p2ptx.clone(),
            &MsgQrx,
            &MsgAtx,
            &parames,
            party_num_int,
            THRESHOLD,
            &party_keys,
            shared_keys.clone(),
            vss_scheme_vec.clone(),
            paillier_key_vector.clone(),
            y_sum,
        );
        if ret == false {
            //we need to clean up all the mesage pool
            let mut iter = MsgQrx.try_iter();
            loop {
                if iter.next().is_none() {
                    println!("empty");
                    break;
                }
            }
            let mut iter = syncrx.try_iter();
            loop {
                if iter.next().is_none() {
                    break;
                }
            }
            let mut iter = tcprx.try_iter();
            loop {
                if iter.next().is_none() {
                    break;
                }
            }
            let mut iter = p2presprx.try_iter();
            loop {
                if iter.next().is_none() {
                    break;
                }
            }
            let signaturepack = LocalMsg {
                Nodeid: parames.mynodeid.to_owned(),
                Msg: "BROKEN SIGNATURE".to_owned(),
            };
            if MsgAtx.send(signaturepack).is_ok() == false {
                println!("check the server connection!");
            } else {
                println!("we have notify the caller of the failure {}", "\u{1f4E2}");
            }
        }
    }
}

#[post("/recvmsg", format = "json", data = "<request>")]
fn ServerRecvMsg(
    request: Json<LocalMsg>,
    chan_mtx: State<Mutex<LocalServerChan>>,
) -> Json<Result<LocalMsg, ()>> {
    let msg: LocalMsg = request.0;
    let msgchan = chan_mtx.lock().unwrap();
    let ret = msgchan.MsgQ.send(msg).is_ok();
    if ret == false {
        return Json(Err(()));
    }

    match msgchan.MsgR.recv() {
        Ok(el) => return Json(Ok(el)),
        Err(_) => {
            println!("error in calculate the result");
            return Json(Err(()));
        }
    };
}

fn RunLocalserver(addr: String, MsgChannel: LocalServerChan) {
    let localserveraddr: Vec<&str> = addr.as_str().split(":").collect();

    let MsgChannel_mtx = Mutex::new(MsgChannel);

    let config = Config::build(Environment::Staging)
        .address(localserveraddr[0])
        .port(localserveraddr[1].parse::<u16>().unwrap())
        .log_level(LoggingLevel::Critical)
        .finalize()
        .unwrap();
    rocket::custom(config)
        .mount("/", routes![ServerRecvMsg])
        .manage(MsgChannel_mtx)
        .launch();
}

fn signmessage(
    tcprx: &mpsc::Receiver<Msg>,
    p2presprx: &Receiver<FloodsubMessage>,
    syncrx: &Receiver<FloodsubMessage>,
    mut p2ptx: tokio::sync::mpsc::Sender<String>,
    MsgQrx: &Receiver<LocalMsg>,
    MsgAtx: &Sender<LocalMsg>,
    parames: &Parameters,
    mut party_num_int: u32,
    THRESHOLD: u32,
    party_keys: &Keys,
    shared_keys: SharedKeys,
    vss_scheme_vec: Vec<VerifiableSS>,
    paillier_key_vector: Vec<EncryptionKey>,
    y_sum: GE,
) -> bool {
    let startemojio = "\u{1f527}";
    println!("{} waiting message to sign...>>>>>>>>>", startemojio);
    let msgpack: LocalMsg;
    match MsgQrx.recv() {
        Ok(v) => {
            msgpack = v;
        }

        Err(e) => {
            let bombemojo = '\u{1F4A3}';
            println!("{} error in waiting for message {:?} ", bombemojo, e);
            return false;
        }
    }
    let message = msgpack.Msg;
    // let message = msgpack.Msg;

    let messagedecode = match base64::decode(&message) {
        Ok(x) => x,
        Err(_e) => message.as_bytes().to_vec(),
    };

    let mut hasher = Sha256::new();

    // write input message
    hasher.input(messagedecode.as_slice());

    let mut msgsha256hash = vec![0; hasher.output_bytes()];
    // read hash digest
    hasher.result(&mut msgsha256hash);
    println!("ready to run");
    let msgbigint = hashToInt(msgsha256hash.as_mut_slice());

    let greetingmsg = GreetingMsg {
        IP: parames.myip.to_owned(),
        Msg: serde_json::to_string(&msgsha256hash).unwrap(),
    };

    // here we go start our rounds!!
    //this is the greeting phase to confirm the participants.
    // we will remove the party_num_int in near feature.
    let msg = Comm::constructmsg(
        "round0",
        "BroadcastTss".to_owned(),
        &parames,
        serde_json::to_string(&greetingmsg).unwrap(),
        parames.mynodeid.to_owned(),
        "All".to_owned(),
    );
    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in zero round");
        return false;
    }

    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round0".to_owned())
        .unwrap_or(HashMap::default());

    if round_ans_vec.is_empty() {
        return false;
    }

    let mut signers: Vec<String> = Vec::new();
    let mut signersseq: Vec<usize> = Vec::new();
    for _ in 0..parames.parties_num {
        signers.push("Empty".to_owned());
    }
    let mut PeerIPmap = HashMap::new();
    let mut messagespool: HashMap<String, Vec<String>> = HashMap::new();
    for (key, value) in round_ans_vec {
        let party_seq: usize = serde_json::from_str(value.key.first.as_str()).unwrap();
        let greetingmsg: GreetingMsg = serde_json::from_str(value.value.as_str()).unwrap();
        if let Some(x) = messagespool.get_mut(&greetingmsg.Msg) {
            x.push(key.to_string());
        } else {
            let mut array = Vec::new();
            let nodeid = key.to_string();
            array.push(nodeid);
            messagespool.insert(greetingmsg.Msg, array);
        }
        let peerip: String = greetingmsg.IP.to_owned();
        let v: Vec<&str> = peerip.split('/').collect();
        let mut addr = v[2].to_owned();
        addr.push(':');
        //fixme default port for TCP
        addr.push_str("3433");
        PeerIPmap.insert(key.to_string(), addr);
        signers[party_seq - 1] = key.to_string();
        signersseq.push(party_seq - 1);
    }
    // we also need to put ourself into the verification pool
    if let Some(x) = messagespool.get_mut(&greetingmsg.Msg) {
        x.push(parames.mynodeid.to_owned());
    } else {
        let mut array = Vec::new();
        array.push(parames.mynodeid.to_owned());
        messagespool.insert(greetingmsg.Msg, array);
    }
    // ensure that we are working on the same message
    let messagecount = messagespool.len();
    if messagecount != 1 {
        return false;
    }

    //we put ourself into the singers vector
    //the signsseq is critical in a ordered way, it defines the postion of the
    //nodes on the curve.
    signers[(party_num_int - 1) as usize] = parames.mynodeid.to_owned();
    signersseq.push((party_num_int - 1) as usize);
    signersseq.sort();
    let myposinseq = signersseq
        .iter()
        .position(|&r| r == (party_num_int - 1) as usize)
        .unwrap();

    party_num_int = party_num_int - 1;

    let private = PartyPrivate::set_private(party_keys.clone(), shared_keys);

    let sign_keys = SignKeys::create(
        &private,
        &vss_scheme_vec[(party_num_int) as usize],
        (party_num_int) as usize,
        &signersseq,
    );

    let xi_com_vec = Keys::get_commitments_to_xi(&vss_scheme_vec);
    //////////////////////////////////////////////////////////////////////////////
    let (com, decommit) = sign_keys.phase1_broadcast();
    let m_a_k = MessageA::a(&sign_keys.k_i, &party_keys.ek);

    //we need to sync with other nodes.

    let syncreply = waitforsync(
        "round0".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round0".to_owned()
        );
        return false;
    }

    let msg = Comm::constructmsg(
        "round1",
        "BroadcastTss".to_owned(),
        &parames,
        serde_json::to_string(&(com.clone(), m_a_k.clone())).unwrap(),
        parames.mynodeid.to_string(),
        "All".to_owned(),
    );
    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round1");
        return false;
    }

    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round1".to_owned())
        .unwrap_or(HashMap::default());
    if round_ans_vec.is_empty() {
        return false;
    }
    let mut bc1_vec: Vec<SignBroadcastPhase1> = Vec::new();
    let mut m_a_vec: Vec<MessageA> = Vec::new();
    for el in signers.iter() {
        if el == "Empty" {
            continue;
        }
        if el.to_owned() == parames.mynodeid {
            bc1_vec.push(com.clone());
            continue;
        }
        let dicdata = round_ans_vec.get(el).unwrap();
        let value = dicdata.value.to_owned();
        let (bc1_j, m_a_party_j): (SignBroadcastPhase1, MessageA) =
            serde_json::from_str(value.as_str()).unwrap();
        bc1_vec.push(bc1_j);
        m_a_vec.push(m_a_party_j);
    }
    assert_eq!(signersseq.len(), bc1_vec.len());

    //////////////////////////////////////////////////////////////////////////////
    let mut m_b_gamma_send_vec: Vec<MessageB> = Vec::new();
    let mut beta_vec: Vec<FE> = Vec::new();
    let mut m_b_w_send_vec: Vec<MessageB> = Vec::new();
    let mut ni_vec: Vec<FE> = Vec::new();
    let mut j = 0;

    for el in signersseq.iter() {
        if el.to_owned() != party_num_int as usize {
            let (m_b_gamma, beta_gamma) = MessageB::b(
                &sign_keys.gamma_i,
                &paillier_key_vector[el.to_owned()],
                m_a_vec[j].clone(),
            );
            let (m_b_w, beta_wi) = MessageB::b(
                &sign_keys.w_i,
                &paillier_key_vector[el.to_owned()],
                m_a_vec[j].clone(),
            );
            m_b_gamma_send_vec.push(m_b_gamma);
            m_b_w_send_vec.push(m_b_w);
            beta_vec.push(beta_gamma);
            ni_vec.push(beta_wi);
            j = j + 1;
        }
    }

    let syncreply = waitforsync(
        "round1".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round1".to_owned()
        );
        return false;
    }

    j = 0;
    for el in signersseq.iter() {
        let to_node = signers[el.to_owned()].to_owned();
        if el.to_owned() != party_num_int as usize {
            let msg = Comm::constructmsg(
                "round2",
                "RESERVERD".to_owned(),
                &parames,
                serde_json::to_string(&(m_b_gamma_send_vec[j].clone(), m_b_w_send_vec[j].clone()))
                    .unwrap(),
                parames.mynodeid.to_owned(),
                to_node.to_owned(),
            );
            let value = signers[el.to_owned()].to_owned();
            let dstip = PeerIPmap.get(&value.to_owned()).unwrap();
            p2pnet::tcp_send(dstip.to_owned(), msg.to_owned());
            j = j + 1;
        }
    }

    let mut round2_dic = HashMap::new();
    loop {
        match tcprx.recv_timeout(Duration::from_millis(WAITTIME)) {
            Ok(resp) => {
                let body: String = resp.Body.to_string();
                let answer: Entry = serde_json::from_str(&body).unwrap();

                round2_dic.insert(answer.key.senderid.to_owned(), answer);
                let length = round2_dic.len();
                if length == THRESHOLD as usize {
                    break;
                }
            }

            Err(e) => {
                let bombemojo = '\u{1F4A3}';
                println!("{} error in round2 p2p send {:?} ", bombemojo, e);
                break;
            }
        }
    }
    if round2_dic.len() != THRESHOLD as usize {
        return false;
    }

    let mut m_b_gamma_rec_vec: Vec<MessageB> = Vec::new();
    let mut m_b_w_rec_vec: Vec<MessageB> = Vec::new();
    for el in signersseq.iter() {
        if el.to_owned() != party_num_int as usize {
            let nodeid = signers[el.to_owned()].to_owned();
            let dicdata = round2_dic.get(&nodeid).unwrap();
            let value = dicdata.value.to_owned();

            let (m_b_gamma_i, m_b_w_i): (MessageB, MessageB) =
                serde_json::from_str(&value).unwrap();
            m_b_gamma_rec_vec.push(m_b_gamma_i);
            m_b_w_rec_vec.push(m_b_w_i);
        }
    }

    let mut alpha_vec: Vec<FE> = Vec::new();
    let mut miu_vec: Vec<FE> = Vec::new();

    let mut j = 0;
    for el in signersseq.iter() {
        if el.to_owned() != party_num_int as usize {
            let m_b = m_b_gamma_rec_vec[j].clone();

            let alpha_ij_gamma = match m_b.verify_proofs_get_alpha(&party_keys.dk, &sign_keys.k_i) {
                Ok(alpha_ij_gamma) => alpha_ij_gamma,
                Err(_) => {
                    println!("wrong dlog or m_b {}", "\u{1f480}");
                    return false;
                }
            };
            let m_b = m_b_w_rec_vec[j].clone();
            let alpha_ij_wi = match m_b.verify_proofs_get_alpha(&party_keys.dk, &sign_keys.k_i) {
                Ok(alpha_ij_wi) => alpha_ij_wi,
                Err(_) => {
                    println!("wrong dlog or m_b {}", "\u{1f480}");
                    return false;
                }
            };
            alpha_vec.push(alpha_ij_gamma);
            miu_vec.push(alpha_ij_wi);
            let g_w_i = Keys::update_commitments_to_xi(
                &xi_com_vec[el.to_owned()],
                &vss_scheme_vec[el.to_owned()],
                el.to_owned(),
                &signersseq,
            );
            assert_eq!(m_b.b_proof.pk.clone(), g_w_i);
            j = j + 1;
        }
    }
    let delta_i = sign_keys.phase2_delta_i(&alpha_vec, &beta_vec);
    let sigma = sign_keys.phase2_sigma_i(&miu_vec, &ni_vec);
    let mymsg = serde_json::to_string(&delta_i).unwrap();

    let syncreply = waitforsync(
        "round2".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round2".to_owned()
        );
        return false;
    }

    let msg = Comm::constructmsg(
        "round3",
        "BroadcastTss".to_owned(),
        &parames,
        mymsg.to_owned(),
        parames.mynodeid.to_string(),
        "All".to_owned(),
    );
    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round1");
        return false;
    }
    let mut round3_vec: Vec<String> = Vec::new();
    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round3".to_owned())
        .unwrap_or(HashMap::default());

    if round_ans_vec.is_empty() {
        return false;
    }
    getsortedresult(
        mymsg.to_owned(),
        party_num_int,
        round_ans_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut round3_vec,
    );
    let mut delta_vec: Vec<FE> = Vec::new();
    if format_vec_from_reads(&round3_vec, &mut delta_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round3".to_owned()
        );
        return false;
    }
    let delta_inv = SignKeys::phase3_reconstruct_delta(&delta_vec);

    let syncreply = waitforsync(
        "round3".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "3".to_owned()
        );
        return false;
    }

    //round 4

    let mut sendmsg = serde_json::to_string(&decommit).unwrap();
    let msg = Comm::constructmsg(
        "round4",
        "BroadcastTss".to_owned(),
        &parames,
        sendmsg.to_owned(),
        parames.mynodeid.to_string(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round1");
        return false;
    }
    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round4".to_owned())
        .unwrap_or(HashMap::default());
    if round_ans_vec.is_empty() {
        return false;
    }
    let mut round4_vec: Vec<String> = Vec::new();
    getsortedresult(
        sendmsg.to_owned(),
        party_num_int,
        round_ans_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut round4_vec,
    );

    let mut decommit_vec: Vec<SignDecommitPhase1> = Vec::new();
    if format_vec_from_reads(&round4_vec, &mut decommit_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round4".to_owned()
        );
        return false;
    }
    let decomm_i = decommit_vec.remove((myposinseq) as usize);

    bc1_vec.remove((myposinseq) as usize);
    let b_proof_vec = (0..m_b_gamma_rec_vec.len())
        .map(|i| &m_b_gamma_rec_vec[i].b_proof)
        .collect::<Vec<&DLogProof>>();
    let R = match SignKeys::phase4(&delta_inv, &b_proof_vec, decommit_vec, &bc1_vec) {
        Ok(R) => R,
        Err(_) => {
            println!("bad gamma_i decommit {}", "\u{1f480}");
            return false;
        }
    };
    let R = R + decomm_i.g_gamma_i * &delta_inv;

    let message_bn = msgbigint;

    let two = BigInt::from(2);
    let message_bn = message_bn.modulus(&two.pow(256));
    let local_sig =
        LocalSignature::phase5_local_sig(&sign_keys.k_i, &message_bn, &R, &sigma, &y_sum);

    let (phase5_com, phase_5a_decom, helgamal_proof) = local_sig.phase5a_broadcast_5b_zkproof();

    let syncreply = waitforsync(
        "round4".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round4".to_owned()
        );
        return false;
    }

    sendmsg = serde_json::to_string(&phase5_com).unwrap();
    let msg = Comm::constructmsg(
        "round5",
        "BroadcastTss".to_owned(),
        &parames,
        sendmsg.to_owned(),
        parames.mynodeid.to_string(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round5");
        return false;
    }

    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round5".to_owned())
        .unwrap_or(HashMap::default());

    if round_ans_vec.is_empty() {
        return false;
    }

    let mut round5_vec: Vec<String> = Vec::new();

    getsortedresult(
        sendmsg.to_owned(),
        party_num_int,
        round_ans_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut round5_vec,
    );

    let mut commit5a_vec: Vec<Phase5Com1> = Vec::new();

    if format_vec_from_reads(&round5_vec, &mut commit5a_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round5".to_owned()
        );
        return false;
    }

    let syncreply = waitforsync(
        "round5".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round5".to_owned()
        );
    }

    sendmsg = serde_json::to_string(&(phase_5a_decom.clone(), helgamal_proof.clone())).unwrap();
    let msg = Comm::constructmsg(
        "round5b",
        "BroadcastTss".to_owned(),
        &parames,
        sendmsg.to_owned(),
        parames.mynodeid.to_string(),
        "All".to_owned(),
    );
    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round5b");
        return false;
    }
    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round5b".to_owned())
        .unwrap_or(HashMap::default());
    if round_ans_vec.is_empty() {
        return false;
    }
    let mut round5b_vec: Vec<String> = Vec::new();

    getsortedresult(
        sendmsg.to_owned(),
        party_num_int,
        round_ans_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut round5b_vec,
    );

    let mut decommit5a_and_elgamal_vec: Vec<(Phase5ADecom1, HomoELGamalProof)> = Vec::new();
    if format_vec_from_reads(&round5b_vec, &mut decommit5a_and_elgamal_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round5b".to_owned()
        );
        return false;
    }
    let decommit5a_and_elgamal_vec_includes_i = decommit5a_and_elgamal_vec.clone();
    decommit5a_and_elgamal_vec.remove((myposinseq) as usize);
    commit5a_vec.remove((myposinseq) as usize);
    let phase_5a_decomm_vec = (0..THRESHOLD)
        .map(|i| decommit5a_and_elgamal_vec[i as usize].0.clone())
        .collect::<Vec<Phase5ADecom1>>();
    let phase_5a_elgamal_vec = (0..THRESHOLD)
        .map(|i| decommit5a_and_elgamal_vec[i as usize].1.clone())
        .collect::<Vec<HomoELGamalProof>>();
    let (phase5_com2, phase_5d_decom2) = match local_sig.phase5c(
        &phase_5a_decomm_vec,
        &commit5a_vec,
        &phase_5a_elgamal_vec,
        &phase_5a_decom.V_i,
        &R.clone(),
    ) {
        Ok((phase5_com2, phase_5d_decom2)) => (phase5_com2, phase_5d_decom2),
        Err(_) => {
            println!("error phase5 {}", "\u{1f480}");
            return false;
        }
    };

    let syncreply = waitforsync(
        "round5b".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round5b".to_owned()
        );
        return false;
    }
    //round 7

    sendmsg = serde_json::to_string(&(phase5_com2)).unwrap();
    let msg = Comm::constructmsg(
        "round7",
        "BroadcastTss".to_owned(),
        &parames,
        sendmsg.to_owned(),
        parames.mynodeid.to_string(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round7");
        return false;
    }
    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round7".to_owned())
        .unwrap_or(HashMap::default());
    if round_ans_vec.is_empty() {
        return false;
    }
    let mut round7_vec: Vec<String> = Vec::new();
    let mut commit5c_vec: Vec<Phase5Com2> = Vec::new();

    getsortedresult(
        sendmsg.to_owned(),
        party_num_int,
        round_ans_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut round7_vec,
    );
    if format_vec_from_reads(&round7_vec, &mut commit5c_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round7".to_owned()
        );
        return false;
    }

    let syncreply = waitforsync(
        "round7".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round7".to_owned()
        );
        return false;
    }
    //round 8

    sendmsg = serde_json::to_string(&(phase_5d_decom2)).unwrap();
    let msg = Comm::constructmsg(
        "round8",
        "BroadcastTss".to_owned(),
        &parames,
        sendmsg.to_owned(),
        parames.mynodeid.to_string(),
        "All".to_owned(),
    );
    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round8");
        return false;
    }
    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round8".to_owned())
        .unwrap_or(HashMap::default());
    if round_ans_vec.is_empty() {
        return false;
    }

    let mut round8_vec: Vec<String> = Vec::new();

    let mut decommit5d_vec: Vec<Phase5DDecom2> = Vec::new();
    getsortedresult(
        sendmsg.to_owned(),
        party_num_int,
        round_ans_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut round8_vec,
    );
    if format_vec_from_reads(&round8_vec, &mut decommit5d_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round8".to_owned()
        );
        return false;
    }

    let phase_5a_decomm_vec_includes_i = (0..THRESHOLD + 1)
        .map(|i| decommit5a_and_elgamal_vec_includes_i[i as usize].0.clone())
        .collect::<Vec<Phase5ADecom1>>();
    let s_i;
    match local_sig.phase5d(
        &decommit5d_vec,
        &commit5c_vec,
        &phase_5a_decomm_vec_includes_i,
    ) {
        Ok(v) => s_i = v, // if Ok(255), set x to 255
        Err(e) => {
            let bombemojo = '\u{1F4A3}';
            println!("{} error in create the signature {:?} ", bombemojo, e);
            return false;
        }
    }

    let syncreply = waitforsync(
        "round8".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round8".to_owned()
        );
        return false;
    }
    // round 9

    sendmsg = serde_json::to_string(&(s_i)).unwrap();
    let msg = Comm::constructmsg(
        "round9",
        "BroadcastTss".to_owned(),
        &parames,
        sendmsg.to_owned(),
        parames.mynodeid.to_string(),
        "All".to_owned(),
    );

    if p2ptx.try_send(msg.into()).is_ok() == false {
        println!("error in send in round1");
        return false;
    }
    let round_ans_vec = Comm::pool_msg((THRESHOLD) as usize, p2presprx, "round9".to_owned())
        .unwrap_or(HashMap::default());
    if round_ans_vec.is_empty() {
        return false;
    }
    let mut round9_vec: Vec<String> = Vec::new();
    let mut s_i_vec: Vec<FE> = Vec::new();
    getsortedresult(
        sendmsg.to_owned(),
        party_num_int,
        round_ans_vec,
        signers.to_owned(),
        signersseq.to_owned(),
        &mut round9_vec,
    );

    if format_vec_from_reads(&round9_vec, &mut s_i_vec).is_ok() == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at {}",
            skullemojio,
            "round9".to_owned()
        );
        return false;
    }

    s_i_vec.remove((myposinseq) as usize);
    let sig = match local_sig.output_signature(&s_i_vec) {
        Ok(sig) => sig,
        Err(_) => {
            println!("verification failed {}", "\u{1f480}");
            return false;
        }
    };

    let msghash = hex::encode(msgsha256hash);
    let signaturereturn = SignaturePack {
        R: BigInt::from(&(sig.r.get_element())[..]).to_str_radix(10),
        S: BigInt::from(&(sig.s.get_element())[..]).to_str_radix(10),
        Pubkeyx: y_sum.x_coor().unwrap().to_str_radix(10),
        Pubkeyy: y_sum.y_coor().unwrap().to_str_radix(10),
        Msghash: msghash,
    };
    let sign_json = serde_json::to_string(&signaturereturn).unwrap();

    let msgencode = base64::encode(&sign_json.to_string());

    let signaturepack = LocalMsg {
        Nodeid: msgpack.Nodeid,
        Msg: msgencode.to_owned(),
    };
    if MsgAtx.send(signaturepack).is_ok() {
        let erroremojio = "\u{1f432}";
        let martini_emoji = "'\u{1F378}";
        println!("{} Tss sign done {}", erroremojio, martini_emoji);
    } else {
        return false;
    }

    let syncreply = waitforsync(
        "round9".to_owned(),
        THRESHOLD as usize,
        syncrx,
        p2ptx.clone(),
        "sync".to_owned(),
    );
    if syncreply == false {
        let skullemojio = "\u{1f480}";
        println!(
            "{} error in signature generation at round {}",
            skullemojio,
            "round9".to_owned()
        );
        return false;
    }

    return true;
}

fn format_vec_from_reads<'a, T: serde::Deserialize<'a> + Clone>(
    ans_vec: &'a Vec<String>,
    new_vec: &'a mut Vec<T>,
) -> Result<bool, serde_json::Error> {
    let mut previous: String = "aa".to_owned();
    for elem in ans_vec.iter() {
        match serde_json::from_str(&elem) {
            Ok(value) => {
                new_vec.push(value);
                previous = elem.to_owned();
            }
            Err(e) => {
                let erroremojio = "\u{1f957}";
                println!(
                    "{} failed to covert string {:?} with error {:?}",
                    erroremojio, elem, e
                );
                println!("correct---->{:?}", previous.to_owned());
                return Err(e);
            }
        }
    }
    return Ok(true);
}

fn getsortedresult(
    myvalue: String,
    party_num_int: u32,
    inputdic: HashMap<String, Entry>,
    signers: Vec<String>,
    signersseq: Vec<usize>,
    ans_vec: &mut Vec<String>,
) {
    for el in signersseq.iter() {
        if el.to_owned() == party_num_int as usize {
            ans_vec.push(myvalue.to_owned());
        } else {
            let nodeid = signers[el.to_owned()].to_owned();
            let dicdata = inputdic.get(&nodeid).unwrap();
            let value = dicdata.value.to_owned();
            ans_vec.push(value);
        }
    }
}

fn waitforsync(
    round: String,
    parties: usize,
    rx: &mpsc::Receiver<FloodsubMessage>,
    mut p2ptx: tokio::sync::mpsc::Sender<String>,
    topic: String,
) -> bool {
    let syncmsg = Comm::syncmsg(round.to_owned(), topic, "ok".to_owned());

    if p2ptx.try_send(syncmsg.into()).is_ok() == false {
        println!("error in send in round1");
        return false;
    }

    return Comm::pool_sync_reply(parties, rx, round.to_owned()).is_ok();
}

fn hashToInt(hash: &mut [u8]) -> BigInt {
    //fixme we set orderbits as 256
    let orderBits = 256;
    let orderBytes = (orderBits + 7) / 8;
    let mut retg;
    if hash.len() > orderBytes {
        let (left, _) = hash.split_at(orderBytes);
        retg = BigInt::from(left);
    } else {
        let hash = &*hash;
        retg = BigInt::from(hash);
    }

    let excess = hash.len() * 8 - orderBits;
    //fixme we rsh the excess.
    if excess > 0 {
        retg = retg >> excess;
    }
    return retg;
}

fn errormsg(msg: String) {
    let bombemojo = '\u{1F4A3}';
    println!("{} error in waiting for message {:?} ", bombemojo, msg);
}
