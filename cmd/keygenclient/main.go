package main

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net/http"
	"os"
	"strings"

	"github.com/binance-chain/go-sdk/common/types"
	"github.com/btcsuite/btcd/btcec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/tendermint/tendermint/crypto/secp256k1"
	"github.com/urfave/cli"
	"gitlab.com/thorchain/bepswap/thornode/bifrost/thorclient"
	"gitlab.com/thorchain/bepswap/thornode/cmd"
)

func main() {
	app := cli.NewApp()
	app.Name = "TSS keygen client"
	app.Usage = "TSS keygen client used to send keygen request to local TSS keygen node"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:     "name",
			Usage:    "name",
			Required: true,
			Hidden:   false,
		},
		cli.StringFlag{
			Name:     "password",
			Usage:    "password",
			Required: true,
			Hidden:   false,
		},
		cli.StringFlag{
			Name:     "url,u",
			Usage:    "url of the tss service",
			EnvVar:   "URL",
			Required: true,
			Hidden:   false,
			Value:    "http://127.0.0.1:8323/recvmsg",
		},
		cli.StringSliceFlag{
			Name:     "pubkey",
			Usage:    "pubkey",
			Required: true,
			Hidden:   false,
		},
	}
	app.Action = appAction
	err := app.Run(os.Args)

	if err != nil {
		log.Fatal(err)
	}
}

func appAction(c *cli.Context) error {
	config := sdk.GetConfig()
	config.SetBech32PrefixForAccount(cmd.Bech32PrefixAccAddr, cmd.Bech32PrefixAccPub)
	config.SetBech32PrefixForValidator(cmd.Bech32PrefixValAddr, cmd.Bech32PrefixValPub)
	config.SetBech32PrefixForConsensusNode(cmd.Bech32PrefixConsAddr, cmd.Bech32PrefixConsPub)
	config.Seal()
	name := c.String("name")
	password := c.String("password")
	k, err := thorclient.NewKeys("", name, password)
	if nil != err {
		return err
	}
	privateKey, err := k.GetPrivateKey()
	if nil != err {
		return err
	}

	fmt.Printf("%T", privateKey)
	pk, _ := privateKey.(secp256k1.PrivKeySecp256k1)

	fmt.Printf("privatekey len(%d)", len(pk))
	hexPrivKey := hex.EncodeToString(pk[:])
	priKey := base64.StdEncoding.EncodeToString([]byte(hexPrivKey))
	addresses := []string{}
	pubkeysInput := c.StringSlice("pubkey")
	pubkeys := []string{}
	for _, item := range pubkeysInput {
		pk, err := sdk.GetAccPubKeyBech32(item)
		if nil != err {
			return err
		}
		addresses = append(addresses, base64.StdEncoding.EncodeToString(pk.Address().Bytes()))
		pubkeys = append(pubkeys, base64.StdEncoding.EncodeToString(pk.Bytes()))
	}
	url := c.String("url")
	if runKeygen(priKey, strings.Join(addresses, ","), strings.Join(pubkeys, ","), url) {
		return nil
	}
	return errors.New("fail to send keygen")
}
func runKeygen(priKey, addresses, pubkey string, url string) bool {
	v := struct {
		PubkeyArrayStr  string
		AddressArrayStr string
		Privkey         string
	}{
		PubkeyArrayStr:  pubkey,
		AddressArrayStr: addresses,
		Privkey:         priKey,
	}
	payload, err := json.Marshal(v)
	if nil != err {
		panic(err)
	}

	// var resptss RetTssKey
	fmt.Println(string(payload))
	fmt.Println("keg gen talk------>", url)
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(payload))
	if err != nil {
		fmt.Println("fail to send to server", err)
		return false
	}
	defer func() {
		if err := resp.Body.Close(); nil != err {
			fmt.Println(err)
		}
	}()

	// Read Response Body
	respbody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return false
	}

	if resp.StatusCode != 200 {
		fmt.Println("error in get the response from Tss keygen error code: ", resp.StatusCode)
		return false
	}
	var dat map[string]interface{}
	err = json.Unmarshal(respbody, &dat)
	if err != nil {
		fmt.Println(err)
		return false
	}

	Tssretmap := dat["Ok"].(map[string]interface{})

	types.Network = types.TestNetwork
	address, err := convertfromTsspubkey(Tssretmap["Pubkeyx"].(string), Tssretmap["Pubkeyy"].(string))
	fmt.Println(address)
	return true
}
func convertfromTsspubkey(x, y string) (types.AccAddress, error) {
	types.Network = types.TestNetwork
	Pubx, _ := new(big.Int).SetString(x, 10)
	Puby, _ := new(big.Int).SetString(y, 10)

	tsspubkey := btcec.PublicKey{
		btcec.S256(),
		Pubx,
		Puby,
	}

	var pubkeyObj secp256k1.PubKeySecp256k1
	copy(pubkeyObj[:], tsspubkey.SerializeCompressed())
	hexPubKey := hex.EncodeToString(pubkeyObj.Address())
	strPubKey, err := sdk.Bech32ifyAccPub(pubkeyObj)
	if nil != err {
		fmt.Println("fail to bech32 account pub key", err)
	}
	fmt.Println(strPubKey)

	address, err := types.AccAddressFromHex(hexPubKey)
	return address, err
}
