FROM rustlang/rust:nightly as builder
RUN apt-get update
RUN apt-get install musl-tools -y
RUN rustup target add x86_64-unknown-linux-musl
RUN apt-get install libgmp3-dev -y
WORKDIR /tss

# copy over your manifests
COPY ./Cargo.toml ./Cargo.toml
COPY ./src ./src
# this build step will cache your dependencies
# Rust cross compile doesn't work , otherwise we could use alpine image
# RUN RUSTFLAGS=-Clinker=musl-gcc cargo build --release --target=x86_64-unknown-linux-musl
RUN cargo build --release

FROM golang:1.13 as gobuilder
ENV CGO_ENABLED=0
ENV GOOS=linux
WORKDIR /app
COPY cmd /app/cmd
WORKDIR /app/cmd/keygenclient
RUN go build

#
# Main
#
FROM ubuntu

RUN apt-get update && \
    apt-get install -y jq

WORKDIR /app
COPY --from=builder  /tss/target/release/tss_keygen .
COPY --from=builder /tss/target/release/tss_sign .
COPY --from=gobuilder /app/cmd/keygenclient/keygenclient .

EXPOSE 5455
EXPOSE 8321
